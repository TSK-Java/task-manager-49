package ru.tsc.kirillov.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.component.MessageExecutor;
import ru.tsc.kirillov.tm.enumerated.EntityOperationType;

import javax.persistence.*;

import static ru.tsc.kirillov.tm.enumerated.EntityOperationType.*;

@NoArgsConstructor
public final class EntityListener {

    @NotNull
    private static final MessageExecutor MESSAGE_EXECUTOR = new MessageExecutor();

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        MESSAGE_EXECUTOR.sendMessage(entity, operationType.toString());
    }

}
