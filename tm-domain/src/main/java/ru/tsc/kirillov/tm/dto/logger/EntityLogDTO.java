package ru.tsc.kirillov.tm.dto.logger;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@XmlRootElement
public class EntityLogDTO implements Serializable {

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final Date date = new Date();

    @NotNull
    private final Object entity;

    @NotNull
    private final String type;

    public EntityLogDTO(
            @NotNull final Object entity,
            @NotNull final String type
    ) {
        this.entity = entity;
        this.type = type;
    }

}
