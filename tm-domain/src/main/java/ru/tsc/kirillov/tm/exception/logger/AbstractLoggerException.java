package ru.tsc.kirillov.tm.exception.logger;

import lombok.NoArgsConstructor;
import ru.tsc.kirillov.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractLoggerException extends AbstractException {

    public AbstractLoggerException(String message) {
        super(message);
    }

    public AbstractLoggerException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractLoggerException(Throwable cause) {
        super(cause);
    }

    public AbstractLoggerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
