package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public class ProjectGetByIndexResponse extends AbstractProjectResponse {

    public ProjectGetByIndexResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
