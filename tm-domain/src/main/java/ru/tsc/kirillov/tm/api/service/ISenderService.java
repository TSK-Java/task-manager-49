package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.logger.EntityLogDTO;

public interface ISenderService {

    void send(@NotNull EntityLogDTO entity);

    EntityLogDTO createMessage(@NotNull Object object, @NotNull String type);

}
