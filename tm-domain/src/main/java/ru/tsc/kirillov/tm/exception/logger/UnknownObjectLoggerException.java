package ru.tsc.kirillov.tm.exception.logger;

import ru.tsc.kirillov.tm.exception.user.AbstractUserException;

public final class UnknownObjectLoggerException extends AbstractUserException {

    public UnknownObjectLoggerException() {
        super("Ошибка! Неизвестный объект логирования.");
    }

}
