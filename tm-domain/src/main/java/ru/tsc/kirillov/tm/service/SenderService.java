package ru.tsc.kirillov.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.service.ISenderService;
import ru.tsc.kirillov.tm.dto.logger.EntityLogDTO;

import javax.jms.*;

public final class SenderService implements ISenderService {

    @NotNull
    private static final String TOPIC_NAME = "TM_LOG";

    @NotNull
    private final ConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull EntityLogDTO entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(TOPIC_NAME);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityLogDTO createMessage(@NotNull Object object, @NotNull String type) {
        @NotNull final EntityLogDTO message = new EntityLogDTO(object, type);
        return message;
    }

}
