package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.IUserServiceDTO;

public interface IServiceLocator {

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserServiceDTO getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}
