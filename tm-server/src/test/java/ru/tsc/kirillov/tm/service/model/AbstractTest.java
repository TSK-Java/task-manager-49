package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.api.service.model.IUserService;
import ru.tsc.kirillov.tm.service.ConnectionService;
import ru.tsc.kirillov.tm.service.PropertyService;

public abstract class AbstractTest {

    @NotNull
    protected static IPropertyService PROPERTY_SERVICE;

    @NotNull
    protected static IConnectionService CONNECTION_SERVICE;

    @NotNull
    protected IUserService userService;

    @BeforeClass
    public static void initConnectionService() {
        PROPERTY_SERVICE = new PropertyService();
        CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);
    }

    @AfterClass
    public static void finalConnectionService() {
        CONNECTION_SERVICE.close();
    }

    @Before
    public void initialization() {
        userService = new UserService(CONNECTION_SERVICE, PROPERTY_SERVICE);
    }

    @After
    public void finalization() {
        userService.clear();
    }

}
