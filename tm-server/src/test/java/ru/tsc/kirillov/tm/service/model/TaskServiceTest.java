package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kirillov.tm.api.service.model.IProjectService;
import ru.tsc.kirillov.tm.api.service.model.ITaskService;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.model.User;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public final class TaskServiceTest extends AbstractUserTest {

    @NotNull 
    private ITaskService service;

    @NotNull
    private IProjectService projectService;
    
    @NotNull
    private final String taskName = UUID.randomUUID().toString();

    @NotNull
    private final String taskDescription = UUID.randomUUID().toString();

    @NotNull
    private final Date dateBegin = new Date();

    @NotNull
    private final Date dateEnd = new Date();

    @NotNull
    private final String taskNewName = UUID.randomUUID().toString();

    @NotNull
    private final String taskNewDescription = UUID.randomUUID().toString();

    @NotNull
    private final Status taskStatus = Status.COMPLETED;

    @Before
    @Override
    public void initialization() {
        super.initialization();
        service = new TaskService(CONNECTION_SERVICE);
        projectService = new ProjectService(CONNECTION_SERVICE);
    }

    @After
    @Override
    public void finalization() {
        service.clear();
        projectService.clear();
        super.finalization();
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.count());
        @NotNull final Task task = new Task(userTest, taskName);
        @Nullable Task taskAdd = service.add(task);
        Assert.assertNotNull(taskAdd);
        Assert.assertEquals(task, taskAdd);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(1, service.count(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.count(""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.count(null));
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, service.count());
        service.create(userId, taskName);
        Assert.assertEquals(1, service.count());
        service.clear();
        Assert.assertEquals(0, service.count());
        service.create(userId, taskName);
        service.create(userAdmin.getId(), taskName);
        Assert.assertEquals(1, service.count(userId));
        Assert.assertEquals(0, service.count(UUID.randomUUID().toString()));
        service.clear(userId);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
        Assert.assertEquals(0, service.count(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(0, service.count());
        service.create(userId, taskName);
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(service.count(), service.findAll().size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        @Nullable final String userIdNull = null;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(userIdNull));
        @NotNull List<Task> tasks = service.findAll(userId);
        Assert.assertEquals(service.count(), tasks.size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(userIdNull, Sort.BY_NAME.getComparator()).size());
        service.clear();
        final int countTask = 10;
        for (int i = 0; i < countTask; i++) {
            @NotNull final String taskName = String.format("Task_%d", countTask - i - 1);
            service.create(userId, taskName);
        }
        tasks = service.findAll(userId, Sort.BY_NAME.getComparator());
        Assert.assertEquals(countTask, tasks.size());
        for (int i = 0; i < countTask; i++) {
            @NotNull final String taskName = String.format("Task_%d", i);
            Assert.assertEquals(taskName, tasks.get(i).getName());
        }
        tasks = service.findAll(userId, Sort.BY_NAME);
        Assert.assertEquals(countTask, tasks.size());
        for (int i = 0; i < countTask; i++) {
            @NotNull final String taskName = String.format("Task_%d", i);
            Assert.assertEquals(taskName, tasks.get(i).getName());
        }
    }

    @Test
    public void existsById() {
        Assert.assertEquals(0, service.count());
        @Nullable final Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertTrue(service.existsById(task.getId()));
        Assert.assertFalse(service.existsById(UUID.randomUUID().toString()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, task.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", task.getId()));
        Assert.assertFalse(service.existsById(UUID.randomUUID().toString(), task.getId()));
        Assert.assertTrue(service.existsById(userId, task.getId()));
        Assert.assertFalse(service.existsById(userId, UUID.randomUUID().toString()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, ""));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, service.count());
        @Nullable final Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertNotNull(service.findOneById(task.getId()));
        Assert.assertNull(service.findOneById(UUID.randomUUID().toString()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, task.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById("", task.getId()));
        Assert.assertNull(service.findOneById(UUID.randomUUID().toString(), task.getId()));
        Assert.assertNotNull(service.findOneById(userId, task.getId()));
        Assert.assertNull(service.findOneById(userId, UUID.randomUUID().toString()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        @Nullable Task taskFind = service.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(0, service.count());
        @Nullable final Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        @Nullable Task taskFind = service.findOneByIndex(0);
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
        taskFind = service.findOneByIndex(userId,0);
        Assert.assertNotNull(taskFind);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex("", 0));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.findOneByIndex(userId, -50));
        Assert.assertNull(service.findOneByIndex(userId, 50));
        Assert.assertNull(service.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, service.count());
        @Nullable Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        @Nullable Task taskRemove = service.remove(task);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(0, service.count());
        Assert.assertEquals(task, taskRemove);
        task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        service.create(userAdmin.getId(), taskName);
        Assert.assertEquals(2, service.count());
        Assert.assertEquals(1, service.count(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, null));
        @NotNull final Task taskFinal = task;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, taskFinal));
        Assert.assertNull(service.remove(userId, null));
        Assert.assertEquals(2, service.count());
        Assert.assertNull(service.remove(UUID.randomUUID().toString(), task));
        Assert.assertEquals(2, service.count());
        taskRemove = service.remove(userId, task);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(task.getId(), taskRemove.getId());
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, service.count());
        @Nullable Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(UUID.randomUUID().toString()));
        Assert.assertEquals(1, service.count());
        @Nullable Task taskRemove = service.removeById(task.getId());
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(task.getId(), taskRemove.getId());
        Assert.assertEquals(0, service.count());
        task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        service.create(userAdmin.getId(), taskName);
        Assert.assertEquals(2, service.count());
        Assert.assertEquals(1, service.count(userId));
        @Nullable final Task taskFinalRemove = task;
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(UUID.randomUUID().toString(), taskFinalRemove.getId())
        );
        Assert.assertEquals(2, service.count());
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(UUID.randomUUID().toString(), UUID.randomUUID().toString())
        );
        Assert.assertEquals(2, service.count());
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(userId, UUID.randomUUID().toString())
        );
        Assert.assertEquals(2, service.count());
        @NotNull final Task taskFinal = task;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, taskFinal.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", taskFinal.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, ""));
        taskRemove = service.removeById(userId, task.getId());
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(task.getId(), taskRemove.getId());
        Assert.assertEquals(1, service.count());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, service.count());
        @Nullable Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex("", null));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(userId, null));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeByIndex(userId, -50));
        Assert.assertNull(service.removeByIndex(userId, 50));
        Assert.assertNull(service.removeByIndex(UUID.randomUUID().toString(), 0));
        @Nullable Task taskRemove = service.removeByIndex(userId, 0);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(0, service.count());
        Assert.assertEquals(task.getId(), taskRemove.getId());
    }

    @Test
    public void count() {
        Assert.assertEquals(0, service.count());
        final int countTask = 10;
        for (int i = 0; i < countTask; i++) {
            @NotNull final String taskName = UUID.randomUUID().toString();
            service.create(userId, taskName);
            service.create(userAdmin.getId(), taskName);
            Assert.assertEquals(i*2 + 2, service.count());
            long cnt = service.count(userId);
            Assert.assertEquals(i + 1, cnt);
            Assert.assertEquals(service.count(), service.findAll().size());
            Assert.assertEquals(service.count(userId), service.findAll(userId).size());
        }
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, ""));
        @Nullable final User userNull = null;
        Assert.assertThrows(UserNotFoundException.class, () -> service.create(userNull, ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertEquals(0, service.count());
        @Nullable Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(1, service.count());
        @Nullable Task taskFind = service.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
        Assert.assertEquals(task.getName(), taskFind.getName());
    }

    @Test
    public void createDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", "", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, "", ""));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, taskName, ""));
        @Nullable final User userNull = null;
        Assert.assertThrows(UserNotFoundException.class, () -> service.create(userNull, "", ""));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, null));
        Assert.assertEquals(0, service.count());
        @Nullable Task task = service.create(userId, taskName, taskDescription);
        Assert.assertNotNull(task);
        Assert.assertEquals(1, service.count());
        @Nullable Task taskFind = service.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
        Assert.assertEquals(task.getName(), taskFind.getName());
        Assert.assertEquals(task.getDescription(), taskFind.getDescription());
    }

    @Test
    public void createDate() {
        Assert.assertEquals(0, service.count());
        @Nullable Task task = service.create(userId, taskName, taskDescription, dateBegin, dateEnd);
        Assert.assertNotNull(task);
        Assert.assertEquals(1, service.count());
        @Nullable Task taskFind = service.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
        Assert.assertEquals(taskName, taskFind.getName());
        Assert.assertEquals(taskDescription, taskFind.getDescription());
        Assert.assertEquals(dateBegin, taskFind.getDateBegin());
        Assert.assertEquals(dateEnd, taskFind.getDateEnd());
    }

    @Test
    public void updateById() {
        @Nullable Task task = service.create(userId, taskName, taskDescription, dateBegin, dateEnd);
        Assert.assertNotNull(task);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById("", "", "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(null, null, null, null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(userId, "", "", "")
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(userId, null, null, null)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(userId, task.getId(), "", "")
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(userId, task.getId(), null, null)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateById(userId, task.getId(), taskNewName, "")
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateById(userId, task.getId(), taskNewName, null)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.updateById(userId, UUID.randomUUID().toString(), taskNewName, taskNewDescription)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.updateById(
                        UUID.randomUUID().toString(), task.getId(), taskNewName, taskNewDescription
                )
        );
        @Nullable Task taskUpdated =
                service.updateById(userId, task.getId(), taskNewName, taskNewDescription);
        Assert.assertNotNull(taskUpdated);
        Assert.assertEquals(task.getId(), taskUpdated.getId());
        Assert.assertEquals(taskNewName, taskUpdated.getName());
        Assert.assertEquals(taskNewDescription, taskUpdated.getDescription());
    }

    @Test
    public void updateByIndex() {
        @Nullable Task task = service.create(userId, taskName, taskDescription, dateBegin, dateEnd);
        Assert.assertNotNull(task);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateByIndex("", null, "", "")
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateByIndex(null, null, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, null, "", "")
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, null, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, -50, "", "")
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.updateByIndex(userId, -50, null, null)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 50, "", "")
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 50, null, null)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 0, "", "")
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateByIndex(userId, 0, null, null)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateByIndex(userId, 0, taskNewName, "")
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> service.updateByIndex(userId, 0, taskNewName, null)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.updateByIndex(UUID.randomUUID().toString(), 0, taskNewName, taskNewDescription)
        );
        @Nullable Task taskUpdated = service.updateByIndex(userId, 0, taskNewName, taskNewDescription);
        Assert.assertNotNull(taskUpdated);
        Assert.assertEquals(task.getId(), taskUpdated.getId());
        Assert.assertEquals(taskNewName, taskUpdated.getName());
        Assert.assertEquals(taskNewDescription, taskUpdated.getDescription());
    }

    @Test
    public void changeStatusByIdAllEmpty() {
        @Nullable Task task = service.create(userId, taskName, taskDescription, dateBegin, dateEnd);
        Assert.assertNotNull(task);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById("", "", null)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById(null, null, null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(userId, "", null)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(userId, null, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusById(userId, task.getId(), null)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.changeStatusById(userId, UUID.randomUUID().toString(), taskStatus)
        );
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.changeStatusById(UUID.randomUUID().toString(), task.getId(), taskStatus)
        );
        @Nullable Task taskChanged = service.changeStatusById(userId, task.getId(), taskStatus);
        Assert.assertNotNull(taskChanged);
        Assert.assertEquals(task.getId(), taskChanged.getId());
        Assert.assertEquals(taskStatus, taskChanged.getStatus());
    }

    @Test
    public void changeStatusByIndexAllEmpty() {
        @Nullable Task task = service.create(userId, taskName, taskDescription, dateBegin, dateEnd);
        Assert.assertNotNull(task);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusByIndex("", null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeStatusByIndex(userId, null, null)
        );
        Assert.assertThrows(
                IndexOutOfBoundsException.class,
                () -> service.changeStatusByIndex(userId, -50, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusByIndex(userId, 50, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusByIndex(userId, 0, null)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusByIndex(UUID.randomUUID().toString(), 0, null)
        );
        @Nullable Task taskChanged = service.changeStatusByIndex(userId,0, taskStatus);
        Assert.assertNotNull(taskChanged);
        Assert.assertEquals(task.getId(), taskChanged.getId());
        Assert.assertEquals(taskStatus, taskChanged.getStatus());
    }

    @Test
    public void removeAllByProjectId() {
        @Nullable final Project project_1 = projectService.create(userId, UUID.randomUUID().toString());
        Assert.assertNotNull(project_1);
        @Nullable final Project project_2 = projectService.create(userAdmin.getId(), UUID.randomUUID().toString());
        Assert.assertNotNull(project_2);
        @Nullable Task task = service.create(userId, taskName);
        Assert.assertNotNull(task);
        task.setProject(project_1);
        service.update(task);
        task = service.create(userAdmin.getId(), taskName);
        Assert.assertNotNull(task);
        task.setProject(project_2);
        service.update(task);

        Assert.assertEquals(2, service.count());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeAllByProjectId(null, null));
        Assert.assertEquals(2, service.count());
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeAllByProjectId(userId, null));
        Assert.assertEquals(2, service.count());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeAllByProjectId(null, project_1.getId()));
        Assert.assertEquals(2, service.count());
        service.removeAllByProjectId(userId, project_1.getId());
        Assert.assertEquals(1, service.count());
    }

}
