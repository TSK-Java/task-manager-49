package ru.tsc.kirillov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.ILoggerService;
import ru.tsc.kirillov.tm.dto.logger.EntityLogDTO;
import ru.tsc.kirillov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class LoggerListener implements MessageListener {

    @NotNull
    final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (entity instanceof EntityLogDTO) loggerService.writeLog((EntityLogDTO) entity);
    }

}
