package ru.tsc.kirillov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.logger.EntityLogDTO;

public interface ILoggerService {

    void writeLog(@NotNull EntityLogDTO message);
    
}
