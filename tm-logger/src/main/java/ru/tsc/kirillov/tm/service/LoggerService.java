package ru.tsc.kirillov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.ILoggerService;
import ru.tsc.kirillov.tm.dto.logger.EntityLogDTO;
import ru.tsc.kirillov.tm.exception.logger.UnknownObjectLoggerException;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String PROJECT_COLLECTION_NAME = "project";

    @NotNull
    private static final String TASK_COLLECTION_NAME = "task";

    @NotNull
    private static final String USER_COLLECTION_NAME = "user";

    @NotNull
    private static final String MONGO_HOST = "localhost";

    private static final int MONGO_PORT = 27017;

    @NotNull
    private static final String MONGO_DB_NAME = "tm_log";

    @NotNull
    @SneakyThrows
    private String getCollectionName(@NotNull final EntityLogDTO message) {
        switch (message.getEntity().getClass().getSimpleName()) {
            case "ProjectDTO":
            case "Project":
                return PROJECT_COLLECTION_NAME;
            case "TaskDTO":
            case "Task":
                return TASK_COLLECTION_NAME;
            case "UserDTO":
            case "User":
                return USER_COLLECTION_NAME;
            default:
                throw new UnknownObjectLoggerException();
        }
    }

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final EntityLogDTO message) {
        @NotNull final String collectionName = getCollectionName(message);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String messageJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(message);
        try (@NotNull final MongoClient client = new MongoClient(MONGO_HOST, MONGO_PORT)) {
            @NotNull final MongoDatabase database = client.getDatabase(MONGO_DB_NAME);
            if (database.getCollection(collectionName) == null) database.createCollection(collectionName);
            @NotNull final MongoCollection<Document> collection = database.getCollection(collectionName);
            @NotNull final Map map = objectMapper.readValue(messageJson, LinkedHashMap.class);
            @NotNull final Document document = new Document(map);
            collection.insertOne(document);
        }
    }

}
